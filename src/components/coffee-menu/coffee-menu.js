import React from 'react';

import './coffee-menu.css';

const CoffeeMenu = props => (
    <div className="coffee-menu">
        <h3 className="coffee-title">Coffee Menu</h3>
        <ul>
            {props.goods.map((goods, index) => {
                return <li key={index} className="coffee-item">
                    <button type="button" className="add-coffee" onClick={props.clickAdd} id={goods.name}>
                        <img src={goods.image} alt="coffee" className="coffee-img"/>
                        <span className="coffee-name">{goods.name}</span>
                        <span className="coffee-price">{'$' + goods.price}</span>
                    </button>
                </li>
            })}
        </ul>

    </div>
);

export default CoffeeMenu;