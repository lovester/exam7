import LatteImage from '../../assets/latte.svg';
import AmericanoImage from '../../assets/americano.svg';
import CappuccinoImage from '../../assets/cappuccino.svg';
import BrewedImage from '../../assets/mug.svg';
import MochaImage from '../../assets/mocha.svg';
import EspressoImage from '../../assets/espresso.svg';

const Goods = () => {
    const GOODS = [
        {name: 'Latte', price: 10, image: LatteImage},
        {name: 'Brewed coffee', price: 8, image: BrewedImage},
        {name: 'Americano', price: 9, image: AmericanoImage},
        {name: 'Espresso', price: 8, image: EspressoImage},
        {name: 'Cappuccino', price: 11, image: CappuccinoImage},
        {name: 'Mocha', price: 12, image: MochaImage}
    ];
    return GOODS;
};

export default Goods;