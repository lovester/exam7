import React from 'react';

import './order.css';

const Order = props => (

    <div className="order-box">
        <div className="order">
            <h2 className="order-title">Order Details:</h2>
            <p className="order-text">Order is empty! Please add some items!</p>
            {props.cof}
            <p className="order-sum">Total price:</p>
            {props.sum}
        </div>

    </div>
);

export default Order;