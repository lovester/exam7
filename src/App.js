import React, { Component } from 'react';
import Goods from './components/goods/goods';
import CoffeeMenu from './components/coffee-menu/coffee-menu';
import Order from './components/order/order';

import './App.css';

class App extends Component {
    state = {
        coffeeItems: [
            {name: 'Latte', count: 0, sum: 0},
            {name: 'Brewed coffee', count: 0, sum: 0},
            {name: 'Americano', count: 0, sum: 0},
            {name: 'Espresso', count: 0, sum: 0},
            {name: 'Cappuccino', count: 0, sum: 0},
            {name: 'Mocha', count: 0, sum: 0}
        ],
        totalSum: 0
    };

    addCoffee = event => {
        const goods = this.state.coffeeItems;
        let totalSum = this.state.totalSum;

        const coffee = goods.find(x => x.name === event.target.id);
        coffee.count++;
        const price = (new Goods()).find(x => x.name === event.target.id).price;
        coffee.sum+=price;
        totalSum+=coffee.sum;

        this.setState({Goods, totalSum});
    };


    render() {
        let coffeeItems = [];

        this.state.coffeeItems.forEach(function (cof) {
            if(cof.count > 0) {
                for(let i = 0; i < cof.count; i++) {
                    coffeeItems.push(<div className={cof.name} key={cof.name+i}/>);
                }
            }
        });

        return (
            <div className="App">
                <div className="container">
                    <Order goods={this.state.coffeeItems} cof={coffeeItems} sum={'$' + this.state.totalSum}/>
                    <CoffeeMenu goods={new Goods()}
                                CoffeeItems={this.state.coffeeItems}
                                clickAdd={(event) => this.addCoffee(event)}/>
                </div>
            </div>
        );
    }
}

export default App;
